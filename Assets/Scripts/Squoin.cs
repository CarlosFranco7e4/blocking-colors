﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Squoin : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            gameManager.SquoinCollected();
            Destroy(gameObject);
        }
    }
}
