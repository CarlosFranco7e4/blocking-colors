﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    private Transform firePoint, weapon;
    [SerializeField]
    private GameObject bulletPrefab, Player;
    private GameObject bullet;
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    private float bulletsPerSecond;
    private float lastShot;
    private float speed = 500f;
    private Vector2 mousePos, dirShoot;

    void Start()
    {
        weapon = GetComponent<Transform>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && (Time.time-lastShot > (1f/bulletsPerSecond)))
        {
            SoundManagerScript.PlaySound("shoot");
            lastShot = Time.time;
            Shoot();
        }
    }

    void Shoot()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        rb = bullet.GetComponent<Rigidbody2D>();
        dirShoot = mousePos - rb.position;
        float angle = Mathf.Atan2(dirShoot.y, dirShoot.x) * Mathf.Rad2Deg;
        weapon.rotation = Quaternion.Euler(0f, 0f, angle);
        rb.AddForce(dirShoot.normalized * speed, ForceMode2D.Force);
    }
}
