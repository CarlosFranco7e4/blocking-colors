﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private int damage = 1;

    private void Update()
    {
        StartCoroutine(DestroyBullet());
    }
    IEnumerator DestroyBullet()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        Personatge personatge = collision.GetComponent<Personatge>();
        if (personatge != null)
        {
            personatge.TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}
