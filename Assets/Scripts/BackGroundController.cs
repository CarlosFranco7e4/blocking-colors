﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGroundController : MonoBehaviour
{
    public float scrollSpeed = 0.025f;

    private Renderer renderer;
    private Vector2 savedOffset;
    public Transform player;

    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    void Update()
    {
        //transform.position = new Vector3(player.position.x + 5, player.position.y, 0);
        float x = Mathf.Repeat(Time.time * scrollSpeed, 1);
        Vector2 offset = new Vector2(x, 0);
        renderer.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}