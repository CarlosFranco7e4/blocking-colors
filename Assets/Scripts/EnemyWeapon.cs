﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    [SerializeField]
    private Transform firePoint, weapon;
    [SerializeField]
    private GameObject bulletPrefab, Boss, Player;
    private GameObject bullet;
    [SerializeField]
    private Rigidbody2D rb;
    [SerializeField]
    public float bulletsPerSecond;
    private float lastShot;
    private float speed = 500f;
    private Vector2 playerPos, dirShoot, bossPos;
    private bool bossShoot;

    void Start()
    {
        weapon = GetComponent<Transform>();
    }

    void Update()
    {

    }
    public void ShootingFirerate()
    {
        if (Player != null && (Time.time - lastShot > (1f / bulletsPerSecond)) && bossShoot == true)
        {
            SoundManagerScript.PlaySound("shoot");
            lastShot = Time.time;
            Shoot();
        }
    }

    void Shoot()
    {
        playerPos = Player.transform.position;
        bossPos = Boss.transform.position;
        dirShoot = playerPos - bossPos; 
        float angle = Mathf.Atan2(dirShoot.y, dirShoot.x) * Mathf.Rad2Deg;
        weapon.rotation = Quaternion.Euler(0f, 0f, angle);
        bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddRelativeForce(new Vector2(1, 0) * speed, ForceMode2D.Force);
    }

    public void ActivateBossFirerate()
    {
        bossShoot = !bossShoot;
    }


}
