﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class PlatformSpawner : MonoBehaviour
{
    public float speedSizeFactor = 0.8f;
    private float leftLimit = -30;
    private float rightLimit = 30;
    private float maxDistanceBetweenPlatforms = 2;
    private float minX = 6;
    private float maxX = 14;
    private float minY = 2;
    private float deltaY = 2;
    private float floorHeight = -6;
    private float lastHeight;
    private float lastWidth;
    public float initialSpeed = -1;
    public float timeFactor = 0.1f;
    public float speed = 0;
    public float startTime;
    public GameObject platform;


    public GameObject enemyPlatform;
    public float enemyProbabilityPercentage = 40;
    public List<GameObject> platforms;




    public State state;
    public enum State
    {
       SwitchPlatform,
       EnemyPlatform

    }

    // Start is called before the first frame update
    private void Awake()
    {
        state = State.SwitchPlatform;
        speed = initialSpeed;
    }
    void Start()
    {
        Random.InitState(18);
        startTime = Time.time;     
        /*for(int i = 0; i < 5; i++)
        {
            platforms.Add(Instantiate(enemy, new Vector3(-6 + (i*2), 0, 0), Quaternion.identity));
            platforms[i].transform.localScale = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY), 1);
        }*/
        platforms.Add(Instantiate(platform, new Vector3(leftLimit - (Mathf.Abs(speed) * speedSizeFactor), floorHeight, 0), Quaternion.identity));
        platforms[0].GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX, maxX), Random.Range(deltaY, deltaY));

    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.SwitchPlatform:

                if (platforms.Last().transform.position.x < rightLimit + (Mathf.Abs(speed) * speedSizeFactor))
                {
                    lastHeight = platforms.Last().GetComponent<SpriteRenderer>().size.y;
                    lastWidth = platforms.Last().GetComponent<SpriteRenderer>().size.x;
                    Debug.Log(lastWidth);
                    platforms.Add(Instantiate(platform, new Vector3(platforms.Last().transform.position.x + lastWidth + Random.Range(0.1f, maxDistanceBetweenPlatforms), floorHeight, 0), Quaternion.identity));
                    platforms.Last().GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX + (Mathf.Abs(speed) * speedSizeFactor), maxX + (Mathf.Abs(speed) * speedSizeFactor)), Random.Range(lastHeight >= deltaY * 2 ? lastHeight - deltaY : lastHeight, lastHeight + deltaY));


                }

                break;
            case State.EnemyPlatform:
                if (platforms.Last().transform.position.x < rightLimit + (Mathf.Abs(speed) * speedSizeFactor))
                {
                    if (Random.Range(0, 101) > (100 - enemyProbabilityPercentage))
                    {
                        lastHeight = platforms.Last().GetComponent<SpriteRenderer>().size.y;
                        lastWidth = platforms.Last().GetComponent<SpriteRenderer>().size.x;
                        Debug.Log(lastWidth);
                        platforms.Add(Instantiate(enemyPlatform, new Vector3(platforms.Last().transform.position.x + lastWidth + Random.Range(0.1f, maxDistanceBetweenPlatforms), floorHeight, 0), Quaternion.identity));
                        platforms.Last().GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX + (Mathf.Abs(speed) * speedSizeFactor), maxX + (Mathf.Abs(speed) * speedSizeFactor)), Random.Range(lastHeight >= deltaY * 2 ? lastHeight - deltaY : lastHeight, lastHeight + deltaY));
                    }
                    else
                    {
                        lastHeight = platforms.Last().GetComponent<SpriteRenderer>().size.y;
                        lastWidth = platforms.Last().GetComponent<SpriteRenderer>().size.x;
                        Debug.Log(lastWidth);
                        platforms.Add(Instantiate(platform, new Vector3(platforms.Last().transform.position.x + lastWidth + Random.Range(0.1f, maxDistanceBetweenPlatforms), floorHeight, 0), Quaternion.identity));
                        platforms.Last().GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX + (Mathf.Abs(speed) * speedSizeFactor), maxX + (Mathf.Abs(speed) * speedSizeFactor)), Random.Range(lastHeight >= deltaY * 2 ? lastHeight - deltaY : lastHeight, lastHeight + deltaY));
                    }
                }
                break;

        }

        /*
        if (platforms.Last().transform.position.x < rightLimit + (Mathf.Abs(speed) * speedSizeFactor))
        {
            if (Random.Range(0, 101) > (100 - enemyProbabilityPercentage))
            {
                lastHeight = platforms.Last().GetComponent<SpriteRenderer>().size.y;
                lastWidth = platforms.Last().GetComponent<SpriteRenderer>().size.x;
                Debug.Log(lastWidth);
                platforms.Add(Instantiate(enemyPlatform, new Vector3(platforms.Last().transform.position.x + lastWidth + Random.Range(0.1f, maxDistanceBetweenPlatforms), floorHeight, 0), Quaternion.identity));
                platforms.Last().GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX + (Mathf.Abs(speed) * speedSizeFactor), maxX + (Mathf.Abs(speed) * speedSizeFactor)), Random.Range(lastHeight >= deltaY * 2 ? lastHeight - deltaY : lastHeight, lastHeight + deltaY));
            }
            else
            {
                lastHeight = platforms.Last().GetComponent<SpriteRenderer>().size.y;
                lastWidth = platforms.Last().GetComponent<SpriteRenderer>().size.x;
                Debug.Log(lastWidth);
                platforms.Add(Instantiate(platform, new Vector3(platforms.Last().transform.position.x + lastWidth + Random.Range(0.1f, maxDistanceBetweenPlatforms), floorHeight, 0), Quaternion.identity));
                platforms.Last().GetComponent<SpriteRenderer>().size = new Vector2(Random.Range(minX + (Mathf.Abs(speed) * speedSizeFactor), maxX + (Mathf.Abs(speed) * speedSizeFactor)), Random.Range(lastHeight >= deltaY * 2 ? lastHeight - deltaY : lastHeight, lastHeight + deltaY));
            }

        }*/
        if (platforms.First().transform.position.x < leftLimit - (Mathf.Abs(speed) * speedSizeFactor))
        {
            Destroy(platforms[0]);
            platforms.RemoveAt(0);
        }
    }
    private void FixedUpdate()
    {
        speed = initialSpeed - (Time.time - startTime) * timeFactor;
        foreach (GameObject platform in platforms)
        {
            platform.GetComponent<PlatformController>().SetVelocity(speed);
        }
        /*foreach (GameObject enemyPlatform in enemyPlatforms)
        {
            enemyPlatform.GetComponent<PlatformController>().SetVelocity(speed);
        }*/
    }
    public void ResetVelocity()
    {
        startTime = Time.time;
    }
}
