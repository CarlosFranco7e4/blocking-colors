﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip squoinSound, hitSound, gameLostSound, gameStartSound,
        jumpSound, longJumpSound, playSound, playerFallSound, playerWinSound, shootSound, deathSound;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        shootSound = Resources.Load<AudioClip>("shoot");
        jumpSound = Resources.Load<AudioClip>("jump");
        longJumpSound = Resources.Load<AudioClip>("longJump");
        squoinSound= Resources.Load<AudioClip>("squoin");
        hitSound = Resources.Load<AudioClip>("hit");
        gameLostSound = Resources.Load<AudioClip>("gameLost");
        gameStartSound = Resources.Load<AudioClip>("gameStart");
        playSound = Resources.Load<AudioClip>("Play");
        playerFallSound = Resources.Load<AudioClip>("playerFall");
        playerWinSound = Resources.Load<AudioClip>("playerWin");
        deathSound = Resources.Load<AudioClip>("deathSound");

        audioSource = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {

    }
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            //checked
            case "shoot":
            audioSource.PlayOneShot(shootSound);
                break;
            //checked
            case "jump":
            audioSource.PlayOneShot(jumpSound);
                break;
            //checked
            case "longJump":
                audioSource.PlayOneShot(longJumpSound);
                break;
            //checked
            case "squoin":
                audioSource.PlayOneShot(squoinSound);
                break;
                //checked
            case "hit":
                audioSource.PlayOneShot(hitSound);
                break;
                //checked
            case "gameLost":
                audioSource.PlayOneShot(gameLostSound);
                break;
            case "gameStart":
                audioSource.PlayOneShot(gameStartSound);
                break;
            case "Play":
                audioSource.PlayOneShot(playSound);
                break;
                //checked
            case "playerFall":
                audioSource.PlayOneShot(playerFallSound);
                break;
                //checked
            case "playerWin":
                audioSource.PlayOneShot(playerWinSound);
                break;
                //checked
            case "deathSound":
                audioSource.PlayOneShot(deathSound);
                break;

        }
    }
    
}
