﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceObserver : MonoBehaviour
{
    public Rigidbody2D player;
    private PlatformSpawner platformSpawner;
    public float totalDistance = 0;
    // Start is called before the first frame update
    void Start()
    {
        platformSpawner = GameObject.FindGameObjectWithTag("PlatformSpawner").GetComponent<PlatformSpawner>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if (player != null && platformSpawner != null && player.velocity.x >= 0)
        {
            totalDistance += Mathf.Abs(Time.fixedDeltaTime * platformSpawner.speed);
            if (totalDistance > PlayerPrefs.GetInt("score"))
            {
                PlayerPrefs.SetInt("score", (int)totalDistance);
            }
            
        }
    }

    public void ResetDistance()
    {
        totalDistance = 0;
    }
}
