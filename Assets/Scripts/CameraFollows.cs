﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollows : MonoBehaviour
{
    public Transform player;
    private float initialPlayerX;
    private void Start()
    {
        initialPlayerX = player.position.x;
    }
    void Update()
    {
        if (player != null)
        {
            transform.position = new Vector3(initialPlayerX + 5, player.position.y, -10);
        }
    }
}
