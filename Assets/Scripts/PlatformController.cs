﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    public float initialSpeed = -1;
    public float timeFactor = 0.1f;
    public float speed = 0;
    public float enemyProbabilityPercentage = 30;
    public float squoinProbabilityPercentage = 40;
    public GameObject enemyPrefab;
    public GameObject squoinPrefab;
    [SerializeField]
    private float squoinMinHeight = 1;
    [SerializeField]
    private float squoinMaxHeight = 5;
    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        if (Random.Range(0,101) > (100-enemyProbabilityPercentage))
        {
            GameObject enemy = Instantiate(enemyPrefab);
            enemy.transform.parent = gameObject.transform;
            enemy.transform.localPosition = new Vector3(Random.Range(0,spriteRenderer.size.x-1), spriteRenderer.size.y, 0);
            enemy.transform.rotation = Quaternion.identity;
        }
        if (Random.Range(0, 101) > (100 - squoinProbabilityPercentage))
        {
            GameObject squoin = Instantiate(squoinPrefab);
            squoin.transform.parent = gameObject.transform;
            squoin.transform.localPosition = new Vector3(Random.Range(0, spriteRenderer.size.x - 1), spriteRenderer.size.y + Random.Range(squoinMinHeight,squoinMaxHeight), 0);
            squoin.transform.rotation = Quaternion.identity;
        }

    }

    // Update is called once per frame
    void Update()
    {
    }
    public void SetVelocity(float speed)
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
    }
}
