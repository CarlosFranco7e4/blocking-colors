﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{

    public List<string> shopItems = new List<string>() { "Glasses" };
    public List<int> itemPrices = new List<int>() { 0 };
    public List<bool> boughtItems = new List<bool>() { false };
    [SerializeField]
    private Text squoinsText;
    [SerializeField]
    private Text glassesText;
    // Start is called before the first frame update
    void Start()
    {
        SyncPrefs();

        //workaround
        if (boughtItems[0] == true)
        {
            glassesText.text = "Bought";
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void BuyObject(string item)
    {
        if (PlayerPrefs.GetInt("squoins") >= 40)
        {
            int index = shopItems.IndexOf(item);
            if (index != -1)
            {
                int squoins = PlayerPrefs.GetInt("squoins");
                int price = itemPrices[index];
                if (squoins >= price && boughtItems[index] != true)
                {
                    Debug.Log("glasses");
                    boughtItems[index] = true;
                    PlayerPrefs.SetInt("squoins", squoins - price);
                    PlayerPrefs.SetInt(item + "Bought", 1);
                    glassesText.text = "Bought"; //workaround
                }
                UpdateSquoins();
            }
        }
    }

    private void SyncPrefs()
    {
        for(int i = 0; i < shopItems.Count; i++)
        {
            if(PlayerPrefs.GetInt(shopItems[i]+"Bought") == 1)
            {
                boughtItems[i] = true;
            }
        }
    }
    private void UpdateSquoins()
    {
        int squoins = PlayerPrefs.GetInt("squoins");
        squoinsText.text = squoins.ToString();
    }
}
