﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public DistanceObserver distance;
    public PlatformSpawner spawner;
    public GameObject gameOver;
    public GameObject gameWon;
    public GameObject metersSquoins;
    public GameObject boss;
    public GameObject gafasPrefab;
    public Personatge player;
    public Camera camera;
    public GameObject health;
    public Text scoreText;
    public Text squoinsText;
    public Text GOSquoins;
    public Text GWSquoins;
    public Text finalScoreTextLose;
    public Text finalScoreTextWin;
    private int squoins;
    private int squoinsTotal;
    private int scoreTotal;
    private bool gafasActive = false;
    public float bossDistanceRequeriment;

    public float RectangliteRequeriment;



    bool gameHasEnded = false;

    // Start is called before the first frame update
    void Start()
    {
        distance = GameObject.FindGameObjectWithTag("DistanceObserver").GetComponent<DistanceObserver>();
        spawner = GameObject.FindGameObjectWithTag("PlatformSpawner").GetComponent<PlatformSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        if(distance != null)
        {
            //score += 1 * Time.deltaTime;//metodo mejorable, faltaria añadir que la puntuacion suba en funcion de la velocidad de "Player"
            //scoreText.text = "Score: " + ((int)score).ToString();
            scoreText.text = " Score\n" + (int)distance.totalDistance;
        }
        ActivateBoss();
        ActivateRectanglite();
    }

    public void ActivateBoss()
    {
        if ((bossDistanceRequeriment <= distance.totalDistance) && !boss.activeSelf)
        {
            Debug.Log("Boss activated");
            boss.SetActive(true);

        }
    }
    public void ActivateRectanglite()
    {
        if (distance.totalDistance >= RectangliteRequeriment)
        {
            spawner.state = PlatformSpawner.State.EnemyPlatform;
            Debug.Log("canvi");
        }
    }


    public float GetCurrentSpeed()
    {
        return Mathf.Abs(spawner.speed);
    }
    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME OVER");
            distance.ResetDistance();
            spawner.ResetVelocity();
            GameOver();
        } 
        void GameOver()
        {

            finalScoreTextLose.text = scoreText.text;
            health.SetActive(false);
            metersSquoins.SetActive(false);
            gameOver.SetActive(true);
            GOSquoins.text = "Squoins:\n" + squoinsText.text;
            squoinsTotal = PlayerPrefs.GetInt("squoins");
            PlayerPrefs.SetInt("squoins", squoins + squoinsTotal);
            /*if (distance.totalDistance > PlayerPrefs.GetInt("score"))
            {
                scoreTotal = PlayerPrefs.GetInt("score");
                PlayerPrefs.SetInt("score", (int)distance.totalDistance + scoreTotal);
            }*/
        }
    }

    public void WinGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Debug.Log("GAME WON");
            distance.ResetDistance();
            spawner.ResetVelocity();
            GameWon();
        }
        void GameWon()
        {
            finalScoreTextWin.text = scoreText.text;
            health.SetActive(false);
            metersSquoins.SetActive(false);
            gameWon.SetActive(true);
            GWSquoins.text = "Squoins:\n" + squoinsText.text;
            squoinsTotal = PlayerPrefs.GetInt("squoins");
            PlayerPrefs.SetInt("squoins", squoins + squoinsTotal);
            PlayerPrefs.SetInt("gameWon", 1);
            /*if (distance.totalDistance > PlayerPrefs.GetInt("score"))
            {
                scoreTotal = PlayerPrefs.GetInt("score");
                PlayerPrefs.SetInt("score", (int)distance.totalDistance + scoreTotal);
            }*/
        }
    }

    public void SquoinCollected()
    {
        SoundManagerScript.PlaySound("squoin");
        squoins++;
        squoinsText.text = squoins.ToString();
    }

    public void PowerUpGafas()
    {
        if (!gafasActive && boss.activeSelf && PlayerPrefs.GetInt("GlassesBought") == 1)
        {
            gafasActive = true;
            GameObject gafas = Instantiate(gafasPrefab, player.gameObject.transform);
            camera.GetComponent<Wilberforce.Colorblind>().Type = 0;
            StartCoroutine(TimeoutPowerUpGafas(5, camera, gafas, this));
        }
    }
    public static IEnumerator TimeoutPowerUpGafas(float time, Camera camera, GameObject gafas, GameManager gm)
    {
        yield return new WaitForSeconds(time);
        Destroy(gafas);
        camera.GetComponent<Wilberforce.Colorblind>().Type = 2;
        gm.gafasActive = false;
    }

}
