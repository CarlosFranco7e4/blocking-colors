﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private int health;
    private int enemiesKilled;
    private int enemiesKilledTotal;
    private Animator animator;
    private bool isDead = false;
    private float deathAnimationSpeed = 2.5f;
    public float dropProbabilityPercentage = 100;
    private GameManager gameManager;
    private void Awake()
    {
        animator = gameObject.GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    // Start is called before the first frame update
    public void TakeDamage(int damage)
    {
        SoundManagerScript.PlaySound("hit");
        health -= damage;

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        if (!isDead)
        {
            enemiesKilled++;
            enemiesKilledTotal = PlayerPrefs.GetInt("enemies");
            PlayerPrefs.SetInt("enemies", enemiesKilled + enemiesKilledTotal);
            if (gameObject.GetComponent<BoxCollider2D>())
            {
                gameObject.GetComponent<BoxCollider2D>().isTrigger=false;
                
            }
            if (gameObject.GetComponent<PolygonCollider2D>())
            {
                isDead = true;
                gameObject.GetComponent<PolygonCollider2D>().enabled = false;
                animator.Play("Death");
                animator.speed = deathAnimationSpeed;
            }
            if(Random.Range(0, 101) > (100 - dropProbabilityPercentage)) {
                gameManager.PowerUpGafas();
            }
            StartCoroutine(DestroyEnemy(animator.GetCurrentAnimatorStateInfo(0).length/deathAnimationSpeed, gameObject));
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

    }

    public static IEnumerator DestroyEnemy(float time, Object ob)
    {
        yield return new WaitForSeconds(time);
        Destroy(ob as GameObject);
    }

}
