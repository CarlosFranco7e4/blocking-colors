﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Personatge : MonoBehaviour
{
    private float jumpDistance;
    [SerializeField]
    private GameObject character;
    [SerializeField]
    private Rigidbody2D rb;
    private Animator animator;
    [SerializeField]
    private SpriteRenderer health;
    [SerializeField]
    private Sprite healthWithOneLess;
    [SerializeField]
    private Sprite healthWithTwoLess;
    private float walkingAnimationSpeedFactor = 0.05f;
    private float walkingAnimationSpeed = 1;
    private bool cannotJump;
    private bool canJump;
    private float timePressed;
    private GameManager gameManager;
    public int lifes;

    private bool isDead = false;

    void Start()
    {
        rb = character.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animator.Play("Walk");
        animator.speed = walkingAnimationSpeed;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            timePressed = Time.time;
        }
        if (Input.GetKeyUp(KeyCode.Space) && canJump)
        {
            if(Time.time - timePressed < 0.25f)
            {
                //jumpSound
                SoundManagerScript.PlaySound("jump");
                jumpDistance = 6.5f;
                canJump = false;
                cannotJump = true;
            }
            else
            {
                SoundManagerScript.PlaySound("longJump");
                jumpDistance = 10;
                canJump = false;
                cannotJump = true;
            }
        }
    }

    private void FixedUpdate()
    {
        if (cannotJump)
        {
            rb.AddForce(Vector2.up * jumpDistance, ForceMode2D.Impulse);
            animator.Play("Jump");
            animator.speed = 0.5f;
            cannotJump = false;
        }
        walkingAnimationSpeed = 2 + gameManager.GetCurrentSpeed() * walkingAnimationSpeedFactor;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        canJump = true;
        animator.Play("Fall");
        animator.speed = 1;
        animator.Play("Walk");
        animator.speed = walkingAnimationSpeed;
    }
    private void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Enemy")
        {
            SoundManagerScript.PlaySound("hit");
            TakeDamage(1);
            if (lifes <= 0)
            {
                SoundManagerScript.PlaySound("deathSound");
                Debug.Log("END");
                Die();
            }

        }

    }

    public void TakeDamage(int damage)
    {
        lifes -= damage;
        if (lifes == 2)
        {
            health.sprite = healthWithOneLess;
        }
        else if (lifes == 1)
        {
            health.sprite = healthWithTwoLess;
        }
        else if (lifes <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        if (!isDead)
        {
            isDead = true;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
            animator.Play("Die");
            StartCoroutine(DestroyPlayer(animator.GetCurrentAnimatorStateInfo(0).length, gameObject));
        }
    }

    public static IEnumerator DestroyPlayer(float time, Object ob)
    {
        yield return new WaitForSeconds(time);
        Destroy(ob as GameObject);
        SoundManagerScript.PlaySound("gameLost");
        FindObjectOfType<GameManager>().EndGame();
    }

}
