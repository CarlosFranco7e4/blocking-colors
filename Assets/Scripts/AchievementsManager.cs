﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementsManager : MonoBehaviour
{
    [SerializeField]
    private GameObject achievement1;
    [SerializeField]
    private GameObject achievement2;
    [SerializeField]
    private GameObject achievement3;
    [SerializeField]
    private GameObject achievement4;
    [SerializeField]
    private GameObject achievement5;
    [SerializeField]
    private GameObject achievement6;
    [SerializeField]
    private GameObject achievement7;
    [SerializeField]
    private GameObject achievement8;
    [SerializeField]
    private GameObject achievement9;
    [SerializeField]
    private GameObject achievement10;
    [SerializeField]
    private GameObject achievement11;

    void Start()
    {
        int score = PlayerPrefs.GetInt("score");
        int enemies = PlayerPrefs.GetInt("enemies");
        int gameWon = PlayerPrefs.GetInt("gameWon");
        if (score > 50)
        {
            achievement1.SetActive(true);
        }
        if (score >= 100)
        {
            achievement2.SetActive(true);
        }
        if (score >= 250)
        {
            achievement3.SetActive(true);
        }
        if (score >= 500)
        {
            achievement4.SetActive(true);
        }
        if (score >= 1000)
        {
            achievement5.SetActive(true);
        }
        if (score >= 2000)
        {
            achievement6.SetActive(true);
        }
        if (enemies >= 1)
        {
            achievement7.SetActive(true);
        }
        if (enemies >= 50)
        {
            achievement8.SetActive(true);
        }
        if (enemies >= 100)
        {
            achievement9.SetActive(true);
        }
        if (enemies >= 500)
        {
            achievement10.SetActive(true);
        }
        if (gameWon == 1)
        {
            achievement11.SetActive(true);
        }
    }
}
