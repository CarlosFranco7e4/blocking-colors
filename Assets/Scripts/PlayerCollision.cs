﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollision : MonoBehaviour
{
    //clase que desactiva el movimiento del jugador y acaba el juego
    //necesita metodos de movement y controller

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Player" )
        {
            SoundManagerScript.PlaySound("playerFall");
            Debug.Log("END");
            FindObjectOfType<GameManager>().EndGame();
        }

    }




}
