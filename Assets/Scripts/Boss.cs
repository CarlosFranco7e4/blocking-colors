﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour
{
    /// <summary>
    /// Esta clase es un gameManager de las mecanicas del jefe,
    /// Entra en escena utilizando el filtro "Deutran"
    /// mientras el jefe no recibe daño dispara constantemente al jugador en un intervalo de 3 segundos
    /// (si es demasiado se sube la constante y ya) hasta que el jefe sea herido.
    /// Cuando el jefe reciva daño este llamará a otros scripts que tiene para gestionar sus mecanicas
    /// si el jefe pierde una vida este llamará a Patrol para que avance a una posición X dispare y 
    /// vuelva a su posición original
    /// si su vida pasa a ser 0 el jefe quitará su habilidad y se irá hacia su punto de origen
    /// 

    
    public int health;
    private bool isDead = false;


    public Animator animator;
    public List <Transform>  target;
    byte nextPosition = 0;
    public float speed;
    [SerializeField]
    EnemyWeapon myEnemyWeapon;

    public Camera camera;
    private DistanceObserver distance;

    State state;
    private enum State
    {  
        StartBoss,
        BasicAttack,
        SpecialAttack,
        DefaultPos,
        BossDefeated
    }
    private void Awake()
    {
        state = State.StartBoss;
    }

    void Update()
    {
        switch (state)
        {
            case State.StartBoss:

                gameObject.GetComponent<CircleCollider2D>().enabled = false;
                transform.position = Vector3.MoveTowards(transform.position, target[nextPosition].transform.position, speed * Time.deltaTime);
                float reachedPositionDistance = 0f;
                if(Vector3.Distance(transform.position, target[nextPosition].transform.position) == reachedPositionDistance)
                {
                    animator.Play("ColorChange");
                    ActivateColorBlindness();
                    state = State.BasicAttack;
                }

                break;
            case State.BasicAttack:
                myEnemyWeapon.bulletsPerSecond = 1f;
                gameObject.GetComponent<CircleCollider2D>().enabled = true;
                myEnemyWeapon.ActivateBossFirerate();
                myEnemyWeapon.ShootingFirerate();
                //animator.Play("Shooting");
                break;
            case State.SpecialAttack:
                transform.position = Vector3.MoveTowards(transform.position, target[nextPosition].transform.position, speed * Time.deltaTime);
                gameObject.GetComponent<CircleCollider2D>().enabled = false;
                animator.SetInteger("position", 1);
                nextPosition = 1;
                float reachedPosition2Distance = 0f;
                if (Vector3.Distance(transform.position, target[nextPosition].transform.position) == reachedPosition2Distance)
                {
                    StartCoroutine(rage());
                }

                break;
            case State.DefaultPos:
                transform.position = Vector3.MoveTowards(transform.position, target[nextPosition].transform.position, speed * Time.deltaTime);
                animator.SetInteger("position", 0);
                nextPosition = 0;
                float backPositionDistance = 0f;
                if (Vector3.Distance(transform.position, target[nextPosition].transform.position) == backPositionDistance)
                {
                    state = State.BasicAttack;
                }
                break;

            case State.BossDefeated:

                transform.position = Vector3.MoveTowards(transform.position, target[nextPosition].transform.position, speed * Time.deltaTime);
                animator.SetInteger("position", 2);
                nextPosition = 2;
                float runawayPositionDistance = 0f;
                if (Vector3.Distance(transform.position, target[nextPosition].transform.position) ==runawayPositionDistance)
                {
                    gameObject.SetActive(false);
                    animator.Play("Death");
                    SoundManagerScript.PlaySound("playerWin");
                    FindObjectOfType<GameManager>().WinGame();
                }
                break;
        }


    }



    public void ActivateColorBlindness()
    {
        camera.GetComponent<Wilberforce.Colorblind>().Type = 2;
    }
    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health % 5 == 0 && health > 0)
        {
            Debug.Log("voy a hacer un ataque especial");
            state = State.SpecialAttack;
        }
        else if (damage % 5 !=0 && health <= 0)
        {
            //la animacion activa patrol para irse y se desactiva de la escena
            state = State.BossDefeated;
        }

    }



    IEnumerator rage()
    {
        myEnemyWeapon.bulletsPerSecond = 25f;
        myEnemyWeapon.ActivateBossFirerate();
        myEnemyWeapon.ShootingFirerate();
        yield return new WaitForSecondsRealtime(0.5f);;
        state = State.DefaultPos;
    }

    /*public static IEnumerator DestroyEnemy(float time, Object ob)
    {
        yield return new WaitForSeconds(1);
        Destroy(ob as GameObject);
    }*/



}
